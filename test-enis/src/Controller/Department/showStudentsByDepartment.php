<?php

namespace App\Controller\Department\showStudentsByDepartment;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class showStudentsByDepartment extends AbstractController
{


    public function __invoke($data)
    {
       return $data->getStudents();
    }
}
